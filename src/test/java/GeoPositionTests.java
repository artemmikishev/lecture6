import org.junit.Assert;
import org.junit.Test;
import ru.edu.lesson6.GeoPosition;

public class GeoPositionTests {

    /**
     * Тест конструктора класса GeoPosition
     */

    /**
     * Тест метода GetLatitude
     */
    @Test
    public void testGetLatitude() {
        GeoPosition geoPosition = new GeoPosition("55(45'07'')", "55(45'07'')");
        double result = ((55 + 45.0/60 + 7.0/3600) * Math.PI )/ 180;
        Assert.assertTrue(geoPosition.getLatitude() - result < 0.00000000000000001);
    }

    /**
     * Тест метода GetLongitude
     */
    @Test
    public void testGetLongitude() {
        GeoPosition geoPosition = new GeoPosition("55(45'07'')", "55(45'07'')");
        double result = ((55 + 45.0/60 + 7.0/3600) * Math.PI )/ 180;
        Assert.assertTrue(geoPosition.getLongitude() - result < 0.00000000000000001);
    }
}
