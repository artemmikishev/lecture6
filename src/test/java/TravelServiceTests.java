import org.junit.Assert;
import org.junit.Test;
import ru.edu.lesson6.CityInfo;
import ru.edu.lesson6.GeoPosition;
import ru.edu.lesson6.TravelService;

import java.util.ArrayList;
import java.util.List;

public class TravelServiceTests {

    private TravelService converter = new TravelService();

    /**
     * Тест метода add, должен добавлять город в список с городами.
     */
    @Test
    public void testAdd() {
        CityInfo moscow = new CityInfo("Москва", new GeoPosition("55(45'07'')", "37(36'56'')"));
        converter.add(moscow);

    }

    /**
     * Тест метода remove, должен удалять город из списка.
     */
    @Test
    public void testRemove() {
        CityInfo moscow = new CityInfo("Москва", new GeoPosition("55(45'07'')", "37(36'56'')"));
        converter.add(moscow);
        converter.remove("Москва");
    }

    /**
     * Тест метода citiesNames, должен возвращать список городов
     */
    @Test
    public void testCitiesNames() {
        CityInfo moscow = new CityInfo("Москва", new GeoPosition("55(45'07'')", "37(36'56'')"));
        CityInfo sankt = new CityInfo("Санкт-Петербург", new GeoPosition("59(46'19'')", "30(18'50'')"));
        CityInfo tver = new CityInfo("Тверь", new GeoPosition("56(51'30'')", "35(54'02'')"));
        List<String> list = new ArrayList<>();
        list.add("Москва");
        list.add("Санкт-Петербург");
        list.add("Тверь");
        Assert.assertTrue(list.containsAll(converter.citiesNames()));
    }

    /**
     * Тест метода getDistance, должен возвращать расстояние между городами указанными в методе.
     * Расстояние между Москвой и Петербургом 622 км, если округлить, согласно сайту:
     * https://planetcalc.ru/73/
     */

    @Test
    public void testGetDistance() {
        CityInfo moscow = new CityInfo("Москва", new GeoPosition("55(45'07'')", "37(36'56'')"));
        CityInfo sankt = new CityInfo("Санкт-Петербург", new GeoPosition("59(46'19'')", "30(18'50'')"));
        converter.add(moscow);
        converter.add(sankt);
        int result = converter.getDistance("Москва", "Санкт-Петербург");
        Assert.assertEquals(result, 622);
    }
    /**
     * Тест метода getCitiesNear должен возвращать список городов в радиусе
     */
    @Test
    public void testGetCitiesNear() {
        CityInfo moscow = new CityInfo("Москва", new GeoPosition("55(45'07'')", "37(36'56'')"));
        CityInfo sankt = new CityInfo("Санкт-Петербург", new GeoPosition("59(46'19'')", "30(18'50'')"));
        CityInfo tver = new CityInfo("Тверь", new GeoPosition("56(51'30'')", "35(54'02'')"));
        converter.add(moscow);
        converter.add(sankt);
        converter.add(tver);
        List<String> result = converter.getCitiesNear("Москва", 1000);
        List<String> isEqual = new ArrayList<>();
        isEqual.add("Санкт-Петербург");
        isEqual.add("Тверь");
        Assert.assertTrue(result.containsAll(isEqual));
    }
    /**
     * Тесты ошибок на методы
     */

    /**
     * Метод add должен вызывать ошибку, если такой город уже есть в списке.
     * IllegalArgumentException
     */
    @Test(expected = IllegalArgumentException.class)
    public void addExceptionTest() {
        CityInfo moscow = new CityInfo("Москва", new GeoPosition("55(45'07'')", "37(36'56'')"));
        CityInfo moscow2 = new CityInfo("Москва", new GeoPosition("55(45'07'')", "37(36'56'')"));
        converter.add(moscow);
        converter.add(moscow2);
    }

    /**
     * Метод remove должен вызывать ошибку, если такого города нет в списке.
     * IllegalArgumentException
     */
    @Test(expected = IllegalArgumentException.class)
    public void removeExceptionTest() {
        converter.remove("Инта");
    }

    /**
     * Метод getDistance должен вызывать ошибку, если хотя бы одного города нет в списке.
     * IllegalArgumentException
     */
    @Test(expected = IllegalArgumentException.class)
    public void getDistanceExceptionTest() {
        CityInfo moscow = new CityInfo("Москва", new GeoPosition("55(45'07'')", "37(36'56'')"));
        CityInfo sankt = new CityInfo("Санкт-Петербург", new GeoPosition("59(46'19'')", "30(18'50'')"));
        CityInfo tver = new CityInfo("Тверь", new GeoPosition("56(51'30'')", "35(54'02'')"));
        converter.add(moscow);
        converter.add(sankt);
        converter.add(tver);
        converter.getDistance("Инта", "Краснодар");

    }

    /**
     * Метод getCitiesNear должен вызывать ошибку, если такого города нет в списке.
     * IllegalArgumentException
     */
    @Test(expected = IllegalArgumentException.class)
    public void getCitiesNearExceptionTest() {
        converter.getCitiesNear("Воркута", 2000);
    }

}
