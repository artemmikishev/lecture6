package ru.edu.lesson6;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Travel Service.
 */
public class TravelService {

    // do not change type
    private final List<CityInfo> cities = new ArrayList<>();

    /**
     * Append city info.
     *
     * @param cityInfo - city info
     * @throws IllegalArgumentException if city already exists
     */
    public void add(CityInfo cityInfo) {
        boolean exists;
        exists = cities.stream()
                .anyMatch(city -> city.getName().equals(cityInfo.getName()));
        if (!exists){
            cities.add(cityInfo);
        } else{
            throw new IllegalArgumentException("City already exists");
        }
        // do something
    }

    /**
     * remove city info.
     *
     * @param cityName - city name
     * @throws IllegalArgumentException if city doesn't exist
     */
    public void remove(String cityName) {
        boolean exists;
        exists = cities.stream()
                .anyMatch(city -> city.getName().equals(cityName));
        if (exists){
            cities.stream()
                    .filter(producer -> producer.getName().equals(cityName))
                    .findFirst()
                    .map(p -> {
                        cities.remove(p);
                        return p;
                    });
        } else
            throw new IllegalArgumentException("City doesn't exist");
    }

    /**
     * Get cities names.
     */
    public List<String> citiesNames() {
        return cities.stream()
                .map(city -> city.getName())
                .collect(Collectors.toList());
    }

    /**
     * Get distance in kilometers between two cities.
     * https://www.kobzarev.com/programming/calculation-of-distances-between-cities-on-their-coordinates/
     *
     * @param srcCityName  - source city
     * @param destCityName - destination city
     * @throws IllegalArgumentException if source or destination city doesn't exist.
     */
    public int getDistance(String srcCityName, String destCityName) {

        boolean exist = cities.stream()
                .anyMatch(city -> city.getName().equals(srcCityName));
        boolean secondExist = cities.stream()
                .anyMatch(city -> city.getName().equals(destCityName));

        if (!exist || !secondExist) {
            throw new IllegalArgumentException("source or destination city doesn't exist");
        }
        CityInfo sourceCity = cities.stream()
                .filter(producer -> producer.getName().equals(srcCityName))
                .findFirst()
                .get();
        CityInfo destCity = cities.stream()
                .filter(producer -> producer.getName().equals(destCityName))
                .findFirst()
                .get();
        double srcLatitude = sourceCity.getPosition().getLatitude();
        double srcLongitude = sourceCity.getPosition().getLongitude();
        double destLatitude = destCity.getPosition().getLatitude();
        double destLongitude = destCity.getPosition().getLongitude();

        double sinSrcLatitude = Math.sin(srcLatitude);
        double sinDestLatitude = Math.sin(destLatitude);
        double cosSrcLatitude = Math.cos(srcLatitude);
        double cosDestLatitude = Math.cos(destLatitude);
        double cosLatLong = Math.cos(srcLongitude - destLongitude);
        double result = Math.acos(sinSrcLatitude * sinDestLatitude + cosSrcLatitude * cosDestLatitude * cosLatLong) * 6372.795;


        return (int) Math.round(result);    }

    /**
     * Get all cities near current city in radius.
     *
     * @param cityName - city
     * @param radius   - radius in kilometers for search
     * @throws IllegalArgumentException if city with cityName city doesn't exist.
     */
    public List<String> getCitiesNear(String cityName, int radius) {
        boolean exists;
        exists = cities.stream()
                .anyMatch(city -> city.getName().equals(cityName));
        if (!exists) {
            throw new IllegalArgumentException("source or destination city doesn't exist");
        }
        List <String> result = cities.stream()
                .map(CityInfo::getName)
                .filter(name -> getDistance(cityName, name) < radius)
                .filter(name -> !name.equals(cityName))
                .collect(Collectors.toList());


        return result;
    }
}
