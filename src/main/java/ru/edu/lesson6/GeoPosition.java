package ru.edu.lesson6;

import java.util.Arrays;
import java.util.stream.Stream;

/**
 * Geo position.
 */
public class GeoPosition {

    /**
     * Широта в радианах.
     */
    private double latitude;

    /**
     * Долгота в радианах.
     */
    private double longitude;

    /**
     * Ctor.
     *
     * @param latitudeGradus  - latitude in gradus
     * @param longitudeGradus - longitude in gradus
     *                        Possible values: 55, 55(45'07''), 59(57'00'')
     */
    public GeoPosition(String latitudeGradus, String longitudeGradus) {

        String[] latituduArray = latitudeGradus.split("[^\\d]+");
        String[] longitudeArray = longitudeGradus.split("[^\\d]+");




        if (longitudeArray.length == 3) {
            this.longitude = (Double.parseDouble(longitudeArray[0]) + Double.parseDouble(longitudeArray[1])/60 + Double.parseDouble(longitudeArray[2])/3600) * Math.PI /180;
        }
        if (longitudeArray.length == 2) {
            this.longitude = (Double.parseDouble(longitudeArray[0]) + Double.parseDouble(longitudeArray[1])/60) * Math.PI /180;;
        }
        if (longitudeArray.length == 1) {
            this.longitude = (Double.parseDouble(longitudeArray[0])) * Math.PI /180;;
        }
        if (latituduArray.length == 3) {
            this.latitude = (Double.parseDouble(latituduArray[0]) + Double.parseDouble(latituduArray[1])/60 + Double.parseDouble(latituduArray[2])/3600) * Math.PI /180;
        }
        if (latituduArray.length == 2) {
            this.latitude = (Double.parseDouble(latituduArray[0]) + Double.parseDouble(latituduArray[1])/60) * Math.PI /180;;
        }
        if (latituduArray.length == 1) {
            this.latitude = (Double.parseDouble(latituduArray[0])) * Math.PI /180;;
        }
        // parse and set latitude and longitude in radian

    }

    // gettes and toString

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    @Override
    public String toString() {
        return "GeoPosition{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
}
