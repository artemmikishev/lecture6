package ru.edu.lesson6;

import java.util.Optional;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        TravelService travelService = new TravelService();
        CityInfo moscow = new CityInfo("Москва", new GeoPosition("55(45'07'')", "37(36'56'')"));
        CityInfo sankt = new CityInfo("Санкт-Петербург", new GeoPosition("59(46'19'')", "30(18'50'')"));
        CityInfo tver = new CityInfo("Тверь", new GeoPosition("56(51'30'')", "35(54'02'')"));
        travelService.add(moscow);
        travelService.add(sankt);
        travelService.add(tver);
        System.out.println(travelService.getDistance("Москва", "Санкт-Петербург"));
        System.out.println(travelService.getCitiesNear("Москва", 700));
        System.out.println(travelService.citiesNames());
        travelService.remove("Санкт-Петербург");
        System.out.println(travelService.getCitiesNear("Москва", 700));
        System.out.println(travelService.citiesNames());

    }
}
